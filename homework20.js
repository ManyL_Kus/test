"use strict";

//Упражнение-1
let i = 0;
//Решение
for (let a = 0; a <= 20; a = a + 2) {
  console.log(a);
}

//Упражнение-2
let sum = 0;
//Решение
for (let i = 0; i < 3; i++) {
  let humber = +prompt("Введите число");

  console.log(humber);

  if (isNaN(humber)) {
    alert("Ошибка. Вы ввели не число");
    break;
  }

  if (humber) {
    sum += humber;
    continue;
  }
}

if (sum) {
  alert("Сумма: " + sum);
}

//Упражнение-3
let months = [
  "Январь",
  "Февраль",
  "Марта",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];
//Решение
function getNameOfMonth(month) {
  return months[month];
}

console.log(getNameOfMonth(1));

for (let i = 0; i < 12; i++) {
  if (months[i] === "9") continue;
  console.log(months[i]);
}

